package books

import (
	"encoding/json"
	"strings"
)

type Book struct {
	Title  string
	Author string
	Pages  int
}

func (b *Book) CategoryByLength() string {
	if b.Pages > 300 {
		return "NOVEL"
	} else {
		return "SHORT STORY"
	}
}

func NewBookFromJSON(jsonString string) Book {
	var book Book
	json.Unmarshal([]byte(jsonString), &book)
	return book
}

func (b *Book) AuthorLastName() string {
	var authorNames []string = strings.Split(b.Author, " ")
	return string(authorNames[1])
}
