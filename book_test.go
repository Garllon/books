package books_test

import (
	. "bitbucket.org/garllon/books"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("Book", func() {

	It("can be load from JSON", func() {

		book := Book{
			Title: "Les Miserables",
		}

		Expect(book.Title).To(Equal("Les Miserables"))
	})

	Describe("Categorizing book length", func() {
		var (
			book Book
			err  error
			json string
		)

		Describe("loading from JSON", func() {
			Context("when the JSON parses succesfully", func() {
				BeforeEach(func() {
					json = `{
						"title":"Der Medicus",
						"author":"Noah Gordon",
						"pages":1677
					}`
					book = NewBookFromJSON(json)
				})

				It("should populate the fields correctly", func() {
					Expect(book.Title).To(Equal("Der Medicus"))
					Expect(book.Author).To(Equal("Noah Gordon"))
					Expect(book.Pages).To(Equal(1677))
				})

				It("should not error", func() {
					Expect(err).NotTo(HaveOccurred())
				})

				It("should correctly identify and return the last name", func() {
					Expect(book.AuthorLastName()).To(Equal("Gordon"))
				})
			})

			Context("when the JSON fails to parse", func() {
				BeforeEach(func() {
					json = `{
						"title":"Les Miserables",
						"author":"Victor Hugo",
						"pages":1488oops
					}`
					book = NewBookFromJSON(json)
				})

				It("should return the zero-value for the book", func() {
					Expect(book).To(BeZero())
				})

				PIt("should error", func() {
					Expect(err).To(HaveOccurred())
				})
			})
		})
	})

	Describe("Categorizing book length", func() {
		var (
			longBook  Book
			shortBook Book
		)

		BeforeEach(func() {
			longBook = Book{
				Title:  "Les Miserables",
				Author: "Victor Hugo",
				Pages:  1488,
			}

			shortBook = Book{
				Title:  "Fox In Socks",
				Author: "Dr. Seuss",
				Pages:  24,
			}
		})

		Context("With more than 300 pages", func() {
			It("should be a novel", func() {
				Expect(longBook.CategoryByLength()).To(Equal("NOVEL"))
			})
		})

		Context("With fewer than 300 pages", func() {
			It("should be a short story", func() {
				Expect(shortBook.CategoryByLength()).To(Equal("SHORT STORY"))
			})
		})

		Context("Thats a falling spec conext", func() {
			It("Should failed", func() {
				// Fail("TEST FAILED")
			})
		})
	})
})
