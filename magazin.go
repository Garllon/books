package books

type Magazin struct {
	Title           string
	PublishingHouse string
	Genre           string
	UnitNumbers     int
}

func (m *Magazin) SizeOfTheCompanyByPublishingHouse() string {
	if m.PublishingHouse == "Springer" {
		return "gross"
	} else {
		return "klein"
	}
}
