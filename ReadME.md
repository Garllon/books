# GinkGo Test Projekt #

This is a Projekt to show how GinkGo is working.

The Link to Ginkgo:
[Link to Ginkgo](http://onsi.github.io/ginkgo/ "Link zur offiziellen Seite vin
Ginkgo")

Die Docs zur Erklärung, welche Methoden es bei GinkGo gibt:
[GinkoDocs](https://godoc.org/github.com/onsi/ginkgo "Link zur Doc Seite von Ginkgo")

How to config the output of Ginkgo:
[GinkgoConfig](https://godoc.org/github.com/onsi/ginkgo/config "Configure your Ginkgo")

You need this packages:

```
go get github.com/onsi/ginkgo/ginkgo
go get github.com/onsi/gomega
```

## Config ginkgo ##

[Doku to configure Ginkgo](https://godoc.org/github.com/onsi/ginkgo/config "Configure your Ginkgo")

Normal the pending spec tree are showing

```
Running Suite: Books Suite
==========================
Random Seed: 1418309455
Will run 10 of 11 specs

•••••
------------------------------
P [PENDING]
Book
/Users/bennet/go/src/bitbucket.org/books/book_test.go:113
  Categorizing book length
  /Users/bennet/go/src/bitbucket.org/books/book_test.go:73
    loading from JSON
    /Users/bennet/go/src/bitbucket.org/books/book_test.go:72
      when the JSON fails to parse
      /Users/bennet/go/src/bitbucket.org/books/book_test.go:71
        should error
        /Users/bennet/go/src/bitbucket.org/books/book_test.go:70
------------------------------
•••••
Ran 10 of 11 Specs in 0.000 seconds
SUCCESS! -- 10 Passed | 0 Failed | 1 Pending | 0 Skipped PASS

Ginkgo ran 1 suite in 594.462373ms
Test Suite Passed
```

With this command the Pending Specs not show the tree

ginkgo -noisyPendings=false

```
Running Suite: Books Suite
==========================
Random Seed: 1418308972
Will run 10 of 11 specs

•••••••P•••
Ran 10 of 11 Specs in 0.000 seconds
SUCCESS! -- 10 Passed | 0 Failed | 1 Pending | 0 Skipped PASS

Ginkgo ran 1 suite in 580.812446ms
Test Suite Passed
```