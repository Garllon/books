package books_test

import (
	. "bitbucket.org/garllon/books"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("Magazin", func() {
	Describe("Size of the Company", func() {
		It("returns great if PublishingHouse is Springer", func() {
			magazin := Magazin{
				Title:           "Bild",
				PublishingHouse: "Springer",
				Genre:           "Mistblatt",
				UnitNumbers:     100,
			}

			Expect(magazin.SizeOfTheCompanyByPublishingHouse()).To(Equal("gross"))
		})

		It("returns klein if PublishingHouse is not Springer", func() {
			magazin := Magazin{
				Title:           "Berliner Zeitung",
				PublishingHouse: "Berliner Zeitungs Verlag",
				Genre:           "Tageszeitung",
				UnitNumbers:     10000,
			}

			Expect(magazin.SizeOfTheCompanyByPublishingHouse()).To(Equal("klein"))
		})
	})
})
